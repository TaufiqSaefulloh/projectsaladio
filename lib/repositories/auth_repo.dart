// ignore_for_file: unused_local_variable

import 'package:firebase_auth/firebase_auth.dart';
// ignore: unnecessary_import
import 'package:firebase_core/firebase_core.dart';

class AuthRepo {
  final _auth = FirebaseAuth.instance;
  Future<void> login({required String email, required String password}) async {
    try {
      final user = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
    } on FirebaseException catch (e) {
      throw e.message ?? 'Kesalahan!';
    } catch (e) {
      // ignore: use_rethrow_when_possible
      throw e;
    }
  }

  Future<void> register(
      {required String email, required String password}) async {
    try {
      final user = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
    } on FirebaseException catch (e) {
      throw e.message ?? 'Kesalahan!';
    } catch (e) {
      // ignore: use_rethrow_when_possible
      throw e;
    }
  }
}
