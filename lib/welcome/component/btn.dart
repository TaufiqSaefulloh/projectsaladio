import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:projectsaladio/UI/Home_page.dart';
import 'package:projectsaladio/repositories/phone_auth.dart';

import '../../UI/Login_page.dart';
import '../../UI/Register_page.dart';

class LoginAndSignupBtn extends StatefulWidget {
  const LoginAndSignupBtn({super.key});
  @override
  State<LoginAndSignupBtn> createState() => _LoginAndSignupBtnState();
}
class _LoginAndSignupBtnState extends State<LoginAndSignupBtn> {
  Future<UserCredential> signInWithGoogle() async {
    final GoogleSignInAccount? gUser = await GoogleSignIn().signIn();
    final GoogleSignInAuthentication gAuth = await gUser!.authentication;
    final credential = GoogleAuthProvider.credential(
      accessToken: gAuth.accessToken,
      idToken: gAuth.idToken,
    );
    return await FirebaseAuth.instance.signInWithCredential(credential).then(
        (value) async => await Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => const HomePage()),
            (route) => false));
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 200),
        SizedBox(
          width: 300.0,
          height: 60.0,
          child: ElevatedButton(
            // ignore: sort_child_properties_last
            child: Text(
              "Log in".toUpperCase(),
              style: const TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'inter'),
            ),

            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return const LoginScreen();
                  },
                ),
              );
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                // ignore: deprecated_member_use
                primary: const Color.fromARGB(255, 44, 102, 202),
                elevation: 0),
          ),
        ),
        const SizedBox(height: 16),
        // ElevatedButton(
        // ignore: sort_child_properties_last
        SizedBox(
          width: 300.0,
          height: 60.0,
          child: ElevatedButton(
            // ignore: sort_child_properties_last
            child: Text(
              "Sign Up".toUpperCase(),
              style: const TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'inter'),
            ),

            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return const RegisterScreen();
                  },
                ),
              );
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                // ignore: deprecated_member_use
                primary: const Color.fromARGB(255, 255, 255, 255),
                elevation: 0),
          ),
        ),
        const SizedBox(
          height: 30.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                signInWithGoogle();
              },
              child: const CircleAvatar(
                radius: 20.0,
                backgroundImage: AssetImage("assets/sign/google.png"),
              ),
            ),
            const SizedBox(
              width: 30.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const PhoneAuthScreen()));

              },
              child: const CircleAvatar(
                radius: 20.0,
                backgroundImage: AssetImage("assets/sign/telephone.png"),
              ),
            )
          ],
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          margin: const EdgeInsets.only(top: 32),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: 'Dengan bergabung dengan Saladio, Anda setuju dengan kami ',
              style: TextStyle(
                  color: const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                  height: 150 / 100),
              children: [
                TextSpan(
                  text: 'Persyaratan layanan',
                  style: TextStyle(
                      color: const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                      fontWeight: FontWeight.w700,
                      height: 150 / 100),
                ),
                TextSpan(
                  text: 'dan ',
                  style: TextStyle(
                      color: const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                      height: 150 / 100),
                ),
                TextSpan(
                  text: 'Kebijakan pribadi.',
                  style: TextStyle(
                      color: const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                      fontWeight: FontWeight.w700,
                      height: 150 / 100),
                ),
              ],
            ),
          ),
        ),
        // ),
      ],
    );
  }
}
