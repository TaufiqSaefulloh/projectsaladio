import 'package:flutter/material.dart';
import '../../constant.dart';

class WelcomeImage extends StatelessWidget {
  const WelcomeImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: defaultPadding * 2),
        Row(
          children: [
            const Spacer(),
            Image.asset(
              'assets/logo.png',
              fit: BoxFit.cover,
              width: 100,
              height: 120,
            ),
            const Spacer(),
          ],
        ),
        const SizedBox(height: defaultPadding * 2),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          margin: const EdgeInsets.only(top: 32),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: 'Selamat Datang di ',
              style: TextStyle(
                  color: const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                  height: 150 / 100),
              children: [
                TextSpan(
                  text: 'Saladio',
                  style: TextStyle(
                      color:
                          const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                      fontWeight: FontWeight.w700,
                      height: 150 / 100),
                ),
              ],
            ),
          ),
        ),
        
      Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          margin: const EdgeInsets.only(top: 15),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Tempat Penjualan ',
                  style: TextStyle(
                      color:
                          const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                      height: 150 / 100),
                ),
                TextSpan(
                  text: 'Salad ',
                  style: TextStyle(
                      color:
                          const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                      fontWeight: FontWeight.w700,
                      height: 150 / 100),
                ),
                TextSpan(
                  text: 'dan ',
                  style: TextStyle(
                      color:
                          const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                      height: 150 / 100),
                ),
                TextSpan(
                  text: 'Minuman',
                  style: TextStyle(
                      color:
                          const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
                      fontWeight: FontWeight.w700,
                      height: 150 / 100),
                ),
              ],
            ),
          ),
        ),


      ],
    );
  }
}
