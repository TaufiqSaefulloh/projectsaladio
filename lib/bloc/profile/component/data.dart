class Data {
  Data({
    required this.namalengkap,
    required this.profil,
    required this.email,
    required this.nohp,
    required this.jeniskelamin,
  });
  String namalengkap;
  String profil;
  String email;
  double nohp;
  String jeniskelamin;
  factory Data.fromJson(Map<String, dynamic> json) => Data(
        namalengkap: json["namalengkap"],
        profil: json["profil"],
        email: json["email"],
        nohp: json["nohp"],
        jeniskelamin: json["jeniskelamin"],
      );
  Map<String, dynamic> toJson() => {
        "namalengkap": namalengkap,
        "profil": profil,
        "email": email,
        "nohp": nohp,
        "jeniskelamin": jeniskelamin,
      };
}

final bioData = [
  Data(
    namalengkap: 'Taufiq Saefulloh',
    profil: 'assets/pp.png',
    email: '20102237@ittelkom-pwt.ac.id',
    nohp: 082137279064,
    jeniskelamin: 'laki-laki',
  )
];
