// ignore_for_file: deprecated_member_use, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:projectsaladio/bloc/profile/component/user_info.dart';

final _firestore = FirebaseFirestore.instance;

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController fullNameController = TextEditingController();
  final TextEditingController genderController = TextEditingController();
  final TextEditingController phoneNumberController = TextEditingController();
  // final TextEditingController emailController = TextEditingController();
  // final TextEditingController jenklController = TextEditingController();
  // final TextEditingController hpController = TextEditingController();
  @override
  Future<void> dispose() async {
    fullNameController.dispose();
    phoneNumberController.dispose();
    genderController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text('Profile Edit'),
        actions: [
          Material(
            color: Colors.blue[800]!.withOpacity(0.3),
            child: const BackButton(color: Colors.white),
          ),
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _firestore.collection('User').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const CircularProgressIndicator();
          }

          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                final fullNameEdc =
                    TextEditingController(text: data['fullName'].toString());
                final phoneNumberEdc =
                    TextEditingController(text: data['PhoneNumber'].toString());
                // final emailEdc =
                //     TextEditingController(text: data['email'].toString());
                final genderEdc =
                    TextEditingController(text: data['gender'].toString());

                return SizedBox(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.9,
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              color: const Color.fromARGB(255, 255, 255,
                                  255), // Tambahkan warna latar belakang di sini
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  UserInfoTile(
                                    margin: const EdgeInsets.only(bottom: 16),
                                    label: 'Nama Pengguna',
                                    value: data['fullName'],
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    valueBackground: const Color.fromARGB(
                                        255, 219, 213, 213),
                                  ),
                                  UserInfoTile(
                                    margin: const EdgeInsets.only(bottom: 16),
                                    label: 'Email',
                                    value: user?.email ?? 'No email available',
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    valueBackground: const Color.fromARGB(
                                        255, 219, 213, 213),
                                  ),
                                  UserInfoTile(
                                    margin: const EdgeInsets.only(bottom: 16),
                                    label: 'Jenis Kelamin',
                                    value: data['gender'],
                                    valueBackground: const Color.fromARGB(
                                        255, 219, 213, 213),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                  ),
                                  UserInfoTile(
                                    margin: const EdgeInsets.only(bottom: 16),
                                    label: 'Nomor Telepon',
                                    value: data['phoneNumber'],
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    valueBackground: const Color.fromARGB(
                                        255, 219, 213, 213),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      GestureDetector(
                          onTap: () {},
                          child: PopupMenuButton<String>(
                            onSelected: (value) {
                              if (value == 'edit') {
                                showModalBottomSheet(
                                    context: context,
                                    isScrollControlled: true,
                                    builder: (context) {
                                      return Padding(
                                        padding: const EdgeInsets.all(16.0),
                                        child: Form(
                                          key: _formKey,
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              TextFormField(
                                                controller: fullNameEdc,
                                              ),
                                              const SizedBox(height: 10.0),
                                              TextFormField(
                                                controller: phoneNumberEdc,
                                              ),
                                              const SizedBox(height: 10.0),
                                              TextFormField(
                                                controller: genderEdc,
                                              ),
                                              const SizedBox(height: 10.0),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      bottom:
                                                          MediaQuery.of(context)
                                                              .viewInsets
                                                              .bottom),
                                                  child: SizedBox(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      child: ElevatedButton(
                                                          onPressed: () async {
                                                            if (_formKey
                                                                .currentState!
                                                                .validate()) {
                                                              try {
                                                                await _firestore
                                                                    .collection(
                                                                        'User')
                                                                    .doc(
                                                                        document
                                                                            .id)
                                                                    .update({
                                                                  'fullName':
                                                                      fullNameEdc
                                                                          .text,
                                                                  'phoneNumber':
                                                                      phoneNumberEdc
                                                                          .text,
                                                                  // 'email':
                                                                  //     emailEdc
                                                                  //         .text,
                                                                  'gender':
                                                                      genderEdc
                                                                          .text,
                                                                  'timestamp':
                                                                      FieldValue
                                                                          .serverTimestamp(),
                                                                });
                                                                // ignore: use_build_context_synchronously
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                  const SnackBar(
                                                                      content: Text(
                                                                          'Profile berhasil diperbarui')),
                                                                );
                                                                // ignore: use_build_context_synchronously
                                                                Navigator.pop(
                                                                    context);
                                                              } catch (e) {
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                  SnackBar(
                                                                      content: Text(
                                                                          '$e')),
                                                                );
                                                              }
                                                            }
                                                          },
                                                          child: const Text(
                                                              'Save'))))
                                            ],
                                          ),
                                        ),
                                      );
                                    });
                              } // else if (value == 'delete') {
                              //   String documentId = document.id;
                              //   _firestore
                              //       .collection('User')
                              //       .doc(documentId)
                              //       .delete();
                              // }
                            },
                            itemBuilder: (BuildContext context) => [
                              const PopupMenuItem<String>(
                                value: 'edit',
                                child: Text('Edit Profile'),
                              ),
                              // const PopupMenuItem<String>(
                              //   value: 'delete',
                              //   child: Text('Hapus'),
                              // ),
                            ],
                            
                            child: const Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(
                                  Icons.edit,
                                  color: Colors.black,
                                ),
                                SizedBox(width: 5),
                                Text(
                                  'Edit',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                          )),
                    ],
                  ),
                );
              }).toList(),
            ),
          );
        },
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     showModalBottomSheet(
      //         context: context,
      //         isScrollControlled: true,
      //         builder: (context) {
      //           return Padding(
      //             padding: const EdgeInsets.all(16.0),
      //             child: Form(
      //               key: _formKey,
      //               child: Column(
      //                 mainAxisSize: MainAxisSize.min,
      //                 children: [
      //                   // TextFormField(
      //                   //     controller: fullNameController,
      //                   //     decoration: const InputDecoration(
      //                   //         hintText: 'Nama Lengkap')),
      //                   // const SizedBox(height: 10.0),
      //                   // // TextFormField(
      //                   // //     controller: emailController,
      //                   // //     decoration:
      //                   // //         const InputDecoration(hintText: 'Email')),
      //                   // const SizedBox(height: 10.0),
      //                   // TextFormField(
      //                   //     controller: genderController,
      //                   //     decoration: const InputDecoration(
      //                   //         hintText: 'Jenis Kelamin')),
      //                   // const SizedBox(height: 10.0),
      //                   // TextFormField(
      //                   //     controller: phoneNumberController,
      //                   //     decoration:
      //                   //         const InputDecoration(hintText: 'Telepone')),
      //                   // const SizedBox(height: 10.0),
      //                   // Padding(
      //                   //     padding: EdgeInsets.only(
      //                   //         bottom:
      //                   //             MediaQuery.of(context).viewInsets.bottom),
      //                   //     child: SizedBox(
      //                   //         width: MediaQuery.of(context).size.width,
      //                   //         child: ElevatedButton(
      //                   //             onPressed: () async {
      //                   //               if (_formKey.currentState!.validate()) {
      //                   //                 try {
      //                   //                   // ignore: unused_local_variable
      //                   //                   DocumentReference docRef =
      //                   //                       await _firestore
      //                   //                           .collection('User')
      //                   //                           .add({
      //                   //                     'fullName': fullNameController.text,
      //                   //                     'phoneNumber': phoneNumberController.text,
      //                   //                     // 'email': emailController.text,
      //                   //                     'gender': genderController.text,
      //                   //                     'timestamp':
      //                   //                         FieldValue.serverTimestamp(),
      //                   //                   });
      //                   //                   // ScaffoldMessenger.of(context)
      //                   //                   //     .showSnackBar(
      //                   //                   //   const SnackBar(
      //                   //                   //       content:
      //                   //                   //           Text('Alamat ditambahkan')),
      //                   //                   // );
      //                   //                   Navigator.pop(context);
      //                   //                 } catch (e) {
      //                   //                   ScaffoldMessenger.of(context)
      //                   //                       .showSnackBar(
      //                   //                     SnackBar(content: Text('$e')),
      //                   //                   );
      //                   //                 }
      //                   //               }
      //                   //             },
      //                   //             child: const Text('Simpan'))))
      //                 ],
      //               ),
      //             ),
      //           );
      //         });
      //   },
      //   // child: const Icon(Icons.add),
      // ),
    );
  }
}
