// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import '../../repositories/auth_repo.dart';
part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit() : super(RegisterInitial());
  final _repo = AuthRepo();
  Future<void> register({required String email, required String password, required String name, required String phone, String? gender}) async {
    emit(RegisterLoading());
    try {
      await _repo.register(email: email, password: password);
      emit(RegisterSuccess('Pendaftaran Berhasil!'));
    } catch (e) {
      // ignore: avoid_print
      print(e);
      emit(RegisterFailure(e.toString()));
    }
  }
}
