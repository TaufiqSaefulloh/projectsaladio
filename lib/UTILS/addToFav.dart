// Model untuk produk di keranjang belanja

// ignore_for_file: file_names

import 'package:projectsaladio/Menu/minuman/menudrink.dart';
import 'package:projectsaladio/Menu/salad/menusalad.dart';

class FavoritesItem {
  final String id;

  FavoritesItem({required this.id});
}

// Daftar produk yang ada di keranjang belanja
final List<FavoritesItem> favoritesItems = [];

// Fungsi untuk menambahkan produk ke keranjang belanja
void addToFav(Food food) {
  final favoritesItem = FavoritesItem(id: food.id);
  favoritesItems.add(favoritesItem);
}

// Fungsi untuk menghapus produk dari keranjang belanja
void removeFromFav(FavoritesItem favoritesItem) {
  favoritesItems.remove(favoritesItem);
}

class FavoritesDrink {
  final String id;

  FavoritesDrink({required this.id});
}

// Daftar produk yang ada di keranjang belanja
final List<FavoritesDrink> favoritesdrinks = [];

// Fungsi untuk menambahkan produk ke keranjang belanja
void addFav(Drink drink) {
  final favoritesdrink = FavoritesDrink(id: drink.id);
  favoritesdrinks.add(favoritesdrink);
}

// Fungsi untuk menghapus produk dari keranjang belanja
void removeFromFavDrink(FavoritesDrink favoritesdrink) {
  favoritesdrinks.remove(favoritesdrink);
}
