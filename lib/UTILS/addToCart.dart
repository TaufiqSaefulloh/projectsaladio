// Model untuk produk di keranjang belanja

// ignore_for_file: file_names

import 'package:projectsaladio/Menu/minuman/menudrink.dart';
import 'package:projectsaladio/Menu/salad/menusalad.dart';

class CartItem {
  final String id;

  CartItem({required this.id});
}

// Daftar produk yang ada di keranjang belanja
final List<CartItem> cartItems = [];

// Fungsi untuk menambahkan produk ke keranjang belanja
void addToCart(Food food) {
  final cartItem = CartItem(id: food.id);
  cartItems.add(cartItem);
}


// Fungsi untuk menghapus produk dari keranjang belanja
void removeFromCart(CartItem cartItem) {
  cartItems.remove(cartItem);
}

class Cartdrink {
  final String id;

  Cartdrink({required this.id});
}
final List<Cartdrink> cartdrinks = [];

void addCart(Drink drink) {
  final cartdrink = Cartdrink(id: drink.id);
  cartdrinks.add(cartdrink);
}

void removeFromCartDrink(Cartdrink cartdrink) {
  cartdrinks.remove(cartdrink);
}
