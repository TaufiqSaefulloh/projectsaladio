// ignore_for_file: file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:projectsaladio/constant.dart';

import '../UTILS/router.dart';
import '../bloc/register/register_cubit.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
  
}

class _RegisterScreenState extends State<RegisterScreen> {
  final emailEdc = TextEditingController();
  final passEdc = TextEditingController();
  final nameEdc = TextEditingController();
  final phoneEdc = TextEditingController();
  String? selectedGender;
  bool passInvisible = false;
  

  late Stream<DocumentSnapshot<Map<String, dynamic>>> userStream;
  late CollectionReference<Map<String, dynamic>> usersCollection;

  @override
  void initState() {
    super.initState();

    // Get the Firestore instance and collection
    final firestoreInstance = FirebaseFirestore.instance;
    usersCollection = firestoreInstance.collection('User');

    // Subscribe to the user document stream
    userStream = usersCollection.doc('User').snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RegisterCubit, RegisterState>(
        listener: (context, state) {
          if (state is RegisterLoading) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(const SnackBar(content: Text('Loading..')));
          }
          if (state is RegisterFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(
                content: Text(state.msg),
                backgroundColor: Colors.red,
              ));
          }
          if (state is RegisterSuccess) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(
                content: Text(state.msg),
                backgroundColor: Colors.green,
              ));
            Navigator.pushNamedAndRemoveUntil(
              context,
              rLogin,
              (route) => false,
            );
          }
        },
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 70),
          child: ListView(
            children: [
              Column(
                children: [
                  const SizedBox(height: defaultPadding * 2),
                  Row(
                    children: [
                      const Spacer(),
                      Image.asset(
                        'assets/logo.png',
                        fit: BoxFit.cover,
                        width: 100,
                        height: 120,
                      ),
                      const Spacer(),
                    ],
                  ),
                  const SizedBox(height: defaultPadding * 2),
                  const Text(
                    "Registrasi",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    "Buat Akun-mu Sekarang!",
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                  const SizedBox(height: defaultPadding * 3),
                ],
              ),
              const SizedBox(height: 25),
              TextFormField(
                controller: nameEdc,
                textInputAction: TextInputAction.next,
                cursorColor: kPrimaryColor,
                decoration: const InputDecoration(
                  hintText: "Nama Lengkap",
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(Icons.person),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              TextFormField(
                controller: emailEdc,
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                cursorColor: kPrimaryColor,
                onSaved: (email) {},
                decoration: const InputDecoration(
                  hintText: "Email",
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(Icons.person),
                  ),
                ),
              ),
              TextFormField(
                controller: phoneEdc,
                keyboardType: TextInputType.phone,
                textInputAction: TextInputAction.next,
                cursorColor: kPrimaryColor,
                decoration: const InputDecoration(
                  hintText: "Nomor Telepon",
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(Icons.phone),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              DropdownButtonFormField<String>(
                value: selectedGender,
                onChanged: (value) {
                  setState(() {
                    selectedGender = value;
                  });
                },
                decoration: const InputDecoration(
                  hintText: "Jenis Kelamin",
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(Icons.person_outline),
                  ),
                ),
                items: const [
                  DropdownMenuItem<String>(
                    value: "Laki-laki",
                    child: Text("Laki-laki"),
                  ),
                  DropdownMenuItem<String>(
                    value: "Perempuan",
                    child: Text("Perempuan"),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              TextFormField(
                controller: passEdc,
                cursorColor: kPrimaryColor,
                textInputAction: TextInputAction.done,
                decoration: InputDecoration(
                  hintText: "Password",
                  prefixIcon: const Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(Icons.lock),
                  ),
                  suffixIcon: IconButton(
                    icon: Icon(
                      passInvisible ? Icons.visibility : Icons.visibility_off,
                    ),
                    onPressed: () {
                      setState(() {
                        passInvisible = !passInvisible;
                      });
                    },
                  ),
                ),
                obscureText: !passInvisible,
              ),
              const SizedBox(height: 10),
              const SizedBox(height: 50),
              SizedBox(
                width: 300.0,
                height: 60.0,
                child: ElevatedButton(
                  onPressed: () async {
                    String fullName = nameEdc.text;
                    String phoneNumber = phoneEdc.text;
                    String gender = selectedGender ?? '';

                    // Validasi input
                    if (fullName.isEmpty ||
                        phoneNumber.isEmpty ||
                        gender.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Mohon lengkapi semua data')),
                      );
                      return;
                    }

                    // Buat objek data yang akan disimpan ke Firestore
                    Map<String, dynamic> userData = {
                      'fullName': fullName,
                      'phoneNumber': phoneNumber,
                      'gender': gender,
                      'timestamp': FieldValue.serverTimestamp(),
                    };

                    // Simpan data ke Firestore
                    try {
                      await usersCollection.doc('User').set(userData);
                      // ignore: use_build_context_synchronously
                      context.read<RegisterCubit>().register(
                            email: emailEdc.text,
                            password: passEdc.text,
                            name: nameEdc.text,
                            phone: phoneEdc.text,
                            gender: selectedGender,
                          );
                    } catch (error) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('Gagal menyimpan data: $error')),
                      );
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color(0xff3D4DE0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  child: const Text(
                    "Register",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("Sudah punya akun ?"),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/login');
                    },
                    child: const Text(
                      "Login",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color(0xff3D4DE0),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
