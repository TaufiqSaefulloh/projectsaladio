// ignore_for_file: file_names, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:projectsaladio/Menu/salad/salad.dart';
import 'package:projectsaladio/UI/Cart_page.dart';
import 'package:projectsaladio/UI/Favorit_page.dart';
import 'package:projectsaladio/UI/Profile_page.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Menu/minuman/drink.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;

  final List<Widget> _pages = [
    const HomeScreen(),
    const CartPage(),
    const FavoritesPage(),
    const ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Beranda',
            backgroundColor: Color.fromARGB(255, 7, 65, 7),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_sharp),
            label: 'Pesanan',
            backgroundColor: Color.fromARGB(255, 7, 65, 7),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favorit',
            backgroundColor: Color.fromARGB(255, 7, 65, 7),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Saya',
            backgroundColor: Color.fromARGB(255, 7, 65, 7),
          ),
        ],
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 9, 80, 9),
          actions: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                'assets/logo.png',
                fit: BoxFit.cover,
              ),
            ),
            const Spacer(),
            IconButton(
              icon: const Icon(
                Icons.location_on,
                color: Color.fromARGB(255, 57, 72, 184),
                size: 25,
              ),
              onPressed: () {
                _openLocation();
              },
            ),
            const SizedBox(
              height: 20,
              child: Center(child: Text('Purwokerto')),
            ),
            const Spacer(),
            IconButton(
              icon: Image.asset('assets/profile/pp.png'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>  const Profillku()),
                );
              },
            ),
          ],
        ),
        body: const TabBarView(
          children: [
            Menu(),
          ],
        ),
      ),
    );
  }

  void _openLocation() async {
    const locationUrl = 'https://goo.gl/maps/c2mNPf4JC11WniRRA';
    if (await canLaunch(locationUrl)) {
      await launch(locationUrl);
    } else {
      throw 'Could not launch $locationUrl';
    }
  }
}


class Menu extends StatelessWidget {
  const Menu({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2, // Jumlah tab yang ingin ditampilkan
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 9, 80, 9),
          title: const Text('Temukan Disini'),
          actions: [
            IconButton(
              icon: const Icon(Icons.search),
              onPressed: () {},
            ),
          ],
          bottom: const TabBar(
            tabs: [
              Tab(text: 'Makanan'),
              Tab(text: 'Minuman'),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            GridFood(),
            GridDrink(),
          ],
        ),
      ),
    );
  }
}


