// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:projectsaladio/Menu/minuman/menudrink.dart';
import 'package:projectsaladio/Menu/salad/menusalad.dart';
import 'package:projectsaladio/UTILS/addToCart.dart';

// class CartPage extends StatelessWidget {
//   const CartPage({super.key});


//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.green[900],
//         title: const Center(
//           child: Text(
//             'Keranjang',
//             style: TextStyle(fontWeight: FontWeight.normal),
//           ),
//         ),
//       ),
//       body: ListView.builder(
//         itemCount: cartItems.length + cartdrinks.length,
//         itemBuilder: (context, index) {
//           if (index < cartItems.length) {
//             final cartItem = cartItems[index];
//             final food =
//                 dummyFoods.firstWhere((food) => food.id == cartItem.id);

//             return ListTile(
//               leading: Image.asset(food.image),
//               title: Text(food.name),
//               subtitle: Text('Price: \$${food.price}'),
//               trailing: IconButton(
//                 icon: const Icon(Icons.delete),
//                 onPressed: () {
//                   // Hapus produk dari keranjang belanja
//                   removeFromCart(cartItem);
//                   showDialog(
//                     context: context,
//                     builder: (context) {
//                       return AlertDialog(
//                         title: const Text('Success'),
//                         content: const Text('Product removed from cart.'),
//                         actions: [
//                           TextButton(
//                             child: const Text('OK'),
//                             onPressed: () {
//                               Navigator.of(context).pop();
//                             },
//                           ),
//                         ],
//                       );
//                     },
//                   );
//                 },
//               ),
//             );
//           } else {
//             final cartIndex = index - cartItems.length;
//             final cartDrink = cartdrinks[cartIndex];
//             final drink =
//                 dummyDrinks.firstWhere((drink) => drink.id == cartDrink.id);

//             return ListTile(
//               leading: Image.asset(drink.image),
//               title: Text(drink.name),
//               subtitle: Text('Price: \$${drink.price}'),
//               trailing: IconButton(
//                 icon: const Icon(Icons.delete),
//                 onPressed: () {
//                   // Remove drink from cart
//                   removeFromCartDrink(cartDrink);
//                   showDialog(
//                     context: context,
//                     builder: (context) {
//                       return AlertDialog(
//                         title: const Text('Success'),
//                         content: const Text('Drink removed from cart.'),
//                         actions: [
//                           TextButton(
//                             child: const Text('OK'),
//                             onPressed: () {
//                               Navigator.of(context).pop();
//                             },
//                           ),
//                         ],
//                       );
//                     },
//                   );
//                 },
//               ),
//             );
//           }
//         },
//       ),
//     );
//   }
// }

class CartPage extends StatelessWidget {
  const CartPage({Key? key}) : super(key: key);

  void clearCart() {
    // Clear the cartItems and cartdrinks lists
    cartItems.clear();
    cartdrinks.clear();
  }

  @override
  Widget build(BuildContext context) {
    int totalPrice = 0; // Initialize total price as 0

    // Calculate total price
    for (final cartItem in cartItems) {
      final food = dummyFoods.firstWhere((food) => food.id == cartItem.id);
      totalPrice += food.price;
    }

    for (final cartDrink in cartdrinks) {
      final drink = dummyDrinks.firstWhere((drink) => drink.id == cartDrink.id);
      totalPrice += drink.price;
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[900],
        title: const Center(
          child: Text(
            'Keranjang',
            style: TextStyle(fontWeight: FontWeight.normal),
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: cartItems.length + cartdrinks.length,
              itemBuilder: (context, index) {
                if (index < cartItems.length) {
                  // Build food list tile
                  final cartItem = cartItems[index];
                  final food =
                      dummyFoods.firstWhere((food) => food.id == cartItem.id);
                  return ListTile(
                    leading: Image.asset(food.image),
                    title: Text(food.name),
                    subtitle: Text('Harga: ${food.price}'),
                    trailing: IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () {
                        // Hapus produk dari keranjang belanja
                        removeFromCart(cartItem);
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text('Success'),
                              content: const Text('Product removed from cart.'),
                              actions: [
                                TextButton(
                                  child: const Text('OK'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                    ),
                  );
                } else {
                  // Build drink list tile
                  final cartIndex = index - cartItems.length;
                  final cartDrink = cartdrinks[cartIndex];
                  final drink = dummyDrinks
                      .firstWhere((drink) => drink.id == cartDrink.id);
                  return ListTile(
                    leading: Image.asset(drink.image),
                    title: Text(drink.name),
                    subtitle: Text('Harga: ${drink.price}'),
                    trailing: IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () {
                        // Hapus minuman dari keranjang belanja
                        removeFromCartDrink(cartDrink);
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text('Success'),
                              content: const Text('Drink removed from cart.'),
                              actions: [
                                TextButton(
                                  child: const Text('OK'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                    ),
                  );
                }
              },
            ),
          ),
          // Display total price
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Total Harga: ${totalPrice.toStringAsFixed(2)}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
          ),
          // Checkout button
          ElevatedButton(
            onPressed: () {
              // Perform checkout logic here
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: const Text('Checkout'),
                    content: const Text('Thank you for your order!'),
                    actions: [
                      TextButton(
                        child: const Text('OK'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          // Clear the cart after successful checkout
                          clearCart();
                          // Navigate back to the home page or desired page
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                      ),
                    ],
                  );
                },
              );
            },
            child: const Text('Checkout'),
          ),
        ],
      ),
    );
  }
}
