// ignore: duplicate_ignore
// ignore: file_names
// ignore_for_file: library_private_types_in_public_api, deprecated_member_use, use_build_context_synchronously, file_names, avoid_print, avoid_unnecessary_containers

import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:projectsaladio/UI/Login_page.dart';
import 'package:projectsaladio/bloc/profile/component/user_info.dart';
import 'package:projectsaladio/bloc/profile/profile_edit.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;


final _firestore = FirebaseFirestore.instance;

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      routes: {
        '/Keamanan & Akun': (context) => const Akun(),
        '/Alamat Saya': (context) => const Alamat(),
        '/Hubungi Kami': (context) => const CallCenter(),
        '/Profil': (context) =>  const Profillku(),
      },
    );
  }
}

class HomePage extends StatelessWidget {
  final List<Map<String, dynamic>> _pages = [
    {
      'title': 'Keamanan & Akun',
      'icon': Icons.security,
      'route': '/Keamanan & Akun',
    },
    {
      'title': 'Alamat Saya',
      'icon': Icons.location_on,
      'route': '/Alamat Saya',
    },
    {
      'title': 'Hubungi Kami',
      'icon': Icons.phone,
      'route': '/Hubungi Kami',
    },
    {
      'title': 'Profil',
      'icon': Icons.person,
      'route': '/Profil',
    },
  ];

  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    final email = user?.email;

    return StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('User').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          }
          if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return const Center(
              child: Text('Data tidak ditemukan'),
            );
          }
          final data = snapshot.data!.docs[0].data() as Map<String, dynamic>;

          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.green[900],
              title: Row(
                children: [
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset('assets/profile/pp.png'),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data['fullName'] ?? 'Guest',
                        style: const TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        email ?? '',
                        style: const TextStyle(
                          fontSize: 12.0,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.green),
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    const BoxShadow(
                      color: Colors.white,
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(-2, 0), // Offset to the left
                    ),
                    BoxShadow(
                      color: Colors.white.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(2, 0), // Offset to the right
                    ),
                  ],
                ),
                child: ListView.builder(
                  itemCount: _pages.length,
                  itemBuilder: (context, index) {
                    return Container(
                      decoration: const BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.grey,
                            width: 1.0,
                          ),
                        ),
                      ),
                      child: ListTile(
                        leading: Icon(_pages[index]['icon']),
                        title: Text(_pages[index]['title']),
                        onTap: () {
                          Navigator.pushNamed(context, _pages[index]['route']);
                        },
                      ),
                    );
                  },
                ),
              ),
            ),
            bottomNavigationBar: Container(
              padding: const EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      title: const Text('Sign Out'),
                      content: const Text('Are you sure you want to sign out?'),
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: const Text('No'),
                        ),
                        TextButton(
                          onPressed: () async {
                            Navigator.of(context).pop();
                            GoogleSignIn().signOut();
                            await FirebaseAuth.instance.signOut();
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const LoginScreen(),
                              ),
                              (route) => false,
                            );
                          },
                          child: const Text('Yes'),
                        ),
                      ],
                    ),
                  );
                },
                child: const Text('Sign Out'),
              ),
            ),
          );
        });
  }
}
// Keamanan dan akun

class Akun extends StatefulWidget {
  const Akun({Key? key}) : super(key: key);

  @override
  State<Akun> createState() => _AkunState();
}

class _AkunState extends State<Akun> {
  final currentPassEdc = TextEditingController();
  final newPassEdc = TextEditingController();
  final confirmPassEdc = TextEditingController();

  bool passInvisible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ubah Kata Sandi'),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.green[900],
        actions: [
          Material(
            color: Colors.blue[800]!.withOpacity(0.3),
            child: const BackButton(color: Colors.white),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              controller: currentPassEdc,
              decoration: const InputDecoration(
                labelText: 'Kata Sandi Saat Ini',
              ),
              obscureText: passInvisible,
            ),
            const SizedBox(height: 10),
            TextFormField(
              controller: newPassEdc,
              decoration: const InputDecoration(
                labelText: 'Kata Sandi Baru',
              ),
              obscureText: passInvisible,
            ),
            const SizedBox(height: 10),
            TextFormField(
              controller: confirmPassEdc,
              decoration: const InputDecoration(
                labelText: 'Konfirmasi Kata Sandi Baru',
              ),
              obscureText: passInvisible,
            ),
            const SizedBox(height: 20),
            Row(
              children: [
                Checkbox(
                  value: passInvisible,
                  onChanged: (value) {
                    setState(() {
                      passInvisible = value!;
                    });
                  },
                ),
                const Text('Tampilkan Kata Sandi'),
              ],
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () async {
                String currentPass = currentPassEdc.text;
                String newPass = newPassEdc.text;
                String confirmPass = confirmPassEdc.text;

                // Validasi input
                if (currentPass.isEmpty ||
                    newPass.isEmpty ||
                    confirmPass.isEmpty) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Mohon lengkapi semua data')),
                  );
                  return;
                }

                // Validasi kata sandi baru
                if (newPass != confirmPass) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                        content: Text('Konfirmasi kata sandi tidak sesuai')),
                  );
                  return;
                }

                try {
                  // Ubah kata sandi menggunakan Firebase Auth
                  User? user = FirebaseAuth.instance.currentUser;
                  if (user != null) {
                    AuthCredential credentials = EmailAuthProvider.credential(
                        email: user.email!, password: currentPass);
                    await user.reauthenticateWithCredential(credentials);
                    await user.updatePassword(newPass);

                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                          content: Text('Kata sandi berhasil diubah')),
                    );
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('User tidak ditemukan')),
                    );
                  }
                } catch (error) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        content: Text('Gagal mengubah kata sandi: $error')),
                  );
                }
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const LoginScreen(),
                  ),
                );
              },
              child: const Text('Simpan'),
            ),
          ],
        ),
      ),
    );
  }
}

// ----------------------------

// Alamat saya

class Alamat extends StatefulWidget {
  const Alamat({Key? key}) : super(key: key);
  @override
  State<Alamat> createState() => _AlamatState();
}

class _AlamatState extends State<Alamat> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController titleController = TextEditingController();
  final TextEditingController hpController = TextEditingController();
  final TextEditingController noteController = TextEditingController();
  @override
  Future<void> dispose() async {
    titleController.dispose();
    hpController.dispose();
    noteController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[900],
        automaticallyImplyLeading: false,
        title: const Text('Alamat Saya'),
        actions: [
          Material(
            color: Colors.blue[800]!.withOpacity(0.3),
            child: const BackButton(color: Colors.white),
          ),
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream:
            _firestore.collection('Alamat').orderBy('timestamp').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const CircularProgressIndicator();
          }
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                final titleEdc =
                    TextEditingController(text: data['title'].toString());
                final hpEdc =
                    TextEditingController(text: data['hp'].toString());
                final noteEdc =
                    TextEditingController(text: data['note'].toString());

                return SizedBox(
                  height: 170.0,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: Text(data['title'],
                                    maxLines: 1,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20.0)),
                              ),
                              GestureDetector(
                                  onTap: () {},
                                  child: PopupMenuButton<String>(
                                    onSelected: (value) {
                                      if (value == 'edit') {
                                        showModalBottomSheet(
                                            context: context,
                                            isScrollControlled: true,
                                            builder: (context) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.all(16.0),
                                                child: Form(
                                                  key: _formKey,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: [
                                                      TextFormField(
                                                        controller: titleEdc,
                                                      ),
                                                      TextFormField(
                                                        controller: hpEdc,
                                                      ),
                                                      const SizedBox(
                                                          height: 10.0),
                                                      SizedBox(
                                                          height: 300,
                                                          child: TextFormField(
                                                            controller: noteEdc,
                                                            maxLines:
                                                                null, // Set this
                                                            expands:
                                                                true, // and this
                                                            keyboardType:
                                                                TextInputType
                                                                    .multiline,
                                                          )),
                                                      Padding(
                                                          padding: EdgeInsets.only(
                                                              bottom: MediaQuery
                                                                      .of(
                                                                          context)
                                                                  .viewInsets
                                                                  .bottom),
                                                          child: SizedBox(
                                                              width:
                                                                  MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                              child:
                                                                  ElevatedButton(
                                                                      onPressed:
                                                                          () async {
                                                                        if (_formKey
                                                                            .currentState!
                                                                            .validate()) {
                                                                          try {
                                                                            await _firestore.collection('Alamat').doc(document.id).update({
                                                                              'title': titleEdc.text,
                                                                              'hp': hpEdc.text,
                                                                              'note': noteEdc.text,
                                                                              'timestamp': FieldValue.serverTimestamp(),
                                                                            });
                                                                            ScaffoldMessenger.of(context).showSnackBar(
                                                                              const SnackBar(content: Text('Note berhasil diperbarui')),
                                                                            );
                                                                            Navigator.pop(context);
                                                                          } catch (e) {
                                                                            ScaffoldMessenger.of(context).showSnackBar(
                                                                              SnackBar(content: Text('$e')),
                                                                            );
                                                                          }
                                                                        }
                                                                      },
                                                                      child: const Text(
                                                                          'Save'))))
                                                    ],
                                                  ),
                                                ),
                                              );
                                            });
                                      } else if (value == 'delete') {
                                        String documentId = document.id;
                                        _firestore
                                            .collection('Alamat')
                                            .doc(documentId)
                                            .delete();
                                      }
                                    },
                                    itemBuilder: (BuildContext context) => [
                                      const PopupMenuItem<String>(
                                        value: 'edit',
                                        child: Text('Edit'),
                                      ),
                                      const PopupMenuItem<String>(
                                        value: 'delete',
                                        child: Text('Hapus'),
                                      ),
                                    ],
                                    child: const Icon(Icons.more_vert_outlined),
                                  ))
                            ],
                          ),
                          const SizedBox(height: 10.0),
                          Text(data['hp'],
                              textAlign: TextAlign.justify,
                              maxLines: 5,
                              style: const TextStyle(fontSize: 17.0)),
                          const SizedBox(height: 10.0),
                          Text(data['note'],
                              textAlign: TextAlign.justify,
                              maxLines: 5,
                              style: const TextStyle(fontSize: 17.0)),
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              builder: (context) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                            controller: titleController,
                            decoration: const InputDecoration(
                                hintText: 'Nama Lengkap')),
                        const SizedBox(height: 10.0),
                        TextFormField(
                            controller: hpController,
                            decoration:
                                const InputDecoration(hintText: 'No Hp')),
                        const SizedBox(height: 10.0),
                        SizedBox(
                            height: 300,
                            child: TextFormField(
                                controller: noteController,
                                maxLines: null, // Set this
                                expands: true, // and this
                                keyboardType: TextInputType.multiline,
                                decoration: const InputDecoration(
                                    hintText: 'Alamat Lengkap', filled: true))),
                        Padding(
                            padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom),
                            child: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: ElevatedButton(
                                    onPressed: () async {
                                      if (_formKey.currentState!.validate()) {
                                        try {
                                          // ignore: unused_local_variable
                                          DocumentReference docRef =
                                              await _firestore
                                                  .collection('Alamat')
                                                  .add({
                                            'title': titleController.text,
                                            'hp': hpController.text,
                                            'note': noteController.text,
                                            'timestamp':
                                                FieldValue.serverTimestamp(),
                                          });
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            const SnackBar(
                                                content:
                                                    Text('Note ditambahkan')),
                                          );
                                          Navigator.pop(context);
                                        } catch (e) {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            SnackBar(content: Text('$e')),
                                          );
                                        }
                                      }
                                    },
                                    child: const Text('Save'))))
                      ],
                    ),
                  ),
                );
              });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}

// ------------------------------

// callcenter

class CallCenter extends StatelessWidget {
  const CallCenter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[900],
        automaticallyImplyLeading: false,
        title: const Text('Hubungi Kami'),
        actions: [
          Material(
            color: Colors.blue[800]!.withOpacity(0.3),
            child: const BackButton(color: Colors.white),
          ),
        ],
      ),
      body: const Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            LogoWidget(),
            SizedBox(height: 16),
            KeteranganWidget(),
            SizedBox(height: 16),
            IconSosialMediaWidget(),
          ],
        ),
      ),
    );
  }
}

class LogoWidget extends StatelessWidget {
  const LogoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset(
        'assets/logo.png',
        fit: BoxFit.cover,
        width: 100,
        height: 120,
      ),
    );
  }
}

class KeteranganWidget extends StatelessWidget {
  const KeteranganWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      margin: const EdgeInsets.only(top: 32),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text:
              'Saladio adalah aplikasi yang digunakan untuk penjualan makanan dan minuman. Nikmati  kemudahan bertransaksi dan fitur Saladio',
          style: TextStyle(
              color: const Color.fromARGB(255, 0, 0, 0).withOpacity(0.6),
              height: 150 / 100),
        ),
      ),
    );
  }
}

class IconSosialMediaWidget extends StatelessWidget {
  const IconSosialMediaWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: const Icon(Icons.facebook),
          onPressed: () {
            _launchURL('https://www.instagram.com/toppic_');
          },
        ),
        IconButton(
          icon: const Icon(Icons.email),
          onPressed: () {
            _launchEmail();
          },
        ),
        IconButton(
          icon: const Icon(Icons.phone),
          onPressed: () {
            _launchWhatsApp();
          },
        ),
      ],
    );
  }
}

void _launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Tidak dapat membuka $url';
  }
}

void _launchWhatsApp() async {
  String phoneNumber = '+6281234567890'; // Ganti dengan nomor WhatsApp Anda
  String message = 'Halo, saya ingin menghubungi Anda'; // Pesan default

  String url =
      'https://wa.me/$phoneNumber?text=${Uri.encodeComponent(message)}';

  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Tidak dapat membuka WhatsApp';
  }
}

void _launchEmail() async {
  String email = 'info@contohemail.com'; // Ganti dengan alamat email Anda
  String subject = 'Hubungi Kami'; // Subjek email default
  String body = 'Halo, saya ingin menghubungi Anda'; // Isi email default

  String url =
      'mailto:$email?subject=${Uri.encodeComponent(subject)}&body=${Uri.encodeComponent(body)}';

  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Tidak dapat membuka aplikasi email';
  }
}
// ------------------------------------

// ---

class Profillku extends StatefulWidget {
  const Profillku({super.key});

  @override
  _ProfillkuState createState() => _ProfillkuState();
}

class _ProfillkuState extends State<Profillku> {
  String? profilePictureURL;
    Future<String?> uploadFileToFirebaseStorage(File file) async {
    try {
      // Mengambil referensi Firebase Storage
      firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance
          .ref()
          .child(
              'User/file.jpg'); // Ganti dengan path penyimpanan yang diinginkan

      // Mengunggah file ke Firebase Storage
      await ref.putFile(file);

      // Mengambil URL unduhan file yang diunggah
      String downloadURL = await ref.getDownloadURL();

      // Mengembalikan URL unduhan file
      return downloadURL;
    } catch (e) {
      // Menangani kesalahan yang mungkin terjadi
      print(e.toString());
      return null;
    }
  }
    void uploadFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    if (result != null) {
      File file = File(result.files.single
          .path!); // Menggunakan `path!` untuk mendapatkan path non-null

      String? downloadURL = await uploadFileToFirebaseStorage(file);

      if (downloadURL != null) {
        // Menampilkan gambar yang diunggah
        setState(() {
          profilePictureURL = downloadURL;
        });
      } else {
        // Gagal mengunggah file
        print('Gagal mengunggah file');
      }
    } else {
      // Pengguna tidak memilih file
      print('Pengguna tidak memilih file');
    }
  }
  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[900],
        automaticallyImplyLeading: false,
        title: const Text('Profil'),
        actions: [
          Material(
            color: Colors.blue[800]!.withOpacity(0.3),
            child: const BackButton(color: Colors.white),
          ),
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('User').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          }
          if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return const Center(
              child: Text('Data tidak ditemukan'),
            );
          }
          final data = snapshot.data!.docs[0].data() as Map<String, dynamic>;

          return ListView(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            children: [
              // Section 1 - Profile Picture Wrapper
              Container(
                color: const Color.fromARGB(255, 9, 80, 9),
                padding: const EdgeInsets.symmetric(vertical: 24),
                child: GestureDetector(
                  onTap: () {
                    uploadFile();
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 130,
                        height: 130,
                        margin: const EdgeInsets.only(bottom: 15),
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(100),
                          image: profilePictureURL != null
                              ? DecorationImage(
                                  image: NetworkImage(profilePictureURL!),
                                  fit: BoxFit.cover,
                                )
                              : const DecorationImage(
                                  image: AssetImage('assets/profile/pp.png'),
                                  fit: BoxFit.cover,
                                ),
                        ),
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Change Profile Picture',
                            style: TextStyle(
                              fontFamily: 'inter',
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(width: 8),
                          Icon(
                            Icons.camera,
                            color: Color.fromARGB(255, 150, 151, 159),
                            size: 25,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),

              // Section 2 - User Info Wrapper
              Container(
                margin: const EdgeInsets.only(top: 24),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    UserInfoTile(
                      margin: const EdgeInsets.only(bottom: 16),
                      label: 'Nama Pengguna',
                      value: data['fullName'] as String? ?? 'Nama Kamu',
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      valueBackground: const Color.fromARGB(255, 219, 213, 213),
                    ),
                    UserInfoTile(
                      margin: const EdgeInsets.only(bottom: 16),
                      label: 'Email',
                      value: user?.email ?? 'No email available',
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      valueBackground: const Color.fromARGB(255, 219, 213, 213),
                    ),
                    UserInfoTile(
                      margin: const EdgeInsets.only(bottom: 16),
                      label: 'Jenis Kelamin',
                      value: data['gender'] as String? ?? 'Jenis Kelamin',
                      valueBackground: const Color.fromARGB(255, 219, 213, 213),
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                    ),
                    UserInfoTile(
                      margin: const EdgeInsets.only(bottom: 16),
                      label: 'Nomor Telepon',
                      value: data['phoneNumber'] as String? ??
                          'Masukan No Telepon',
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      valueBackground: const Color.fromARGB(255, 219, 213, 213),
                    ),
                    // Section 3 - Bottom Button
                    Container(
                      margin: const EdgeInsets.only(top: 24),
                      width: double.infinity,
                      // padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const ProfileScreen()),
                          );
                        },
                        child: const Text('Edit Profil'),
                      ),
                    )
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}




  // class Profillku extends StatelessWidget {
  //   const Profillku({super.key});

  //   @override
  //   Widget build(BuildContext context) {
  //     return Scaffold(
  //       appBar: AppBar(
  //         title: const Text('Profil'),
  //       ),
  //       body: ListView(
  //         shrinkWrap: true,
  //         physics: const BouncingScrollPhysics(),
  //         children: [
  //           // Section 1 - Profile Picture Wrapper
  //           Container(
  //             color: const Color.fromARGB(255, 9, 80, 9),
  //             padding: const EdgeInsets.symmetric(vertical: 24),
  //             child: GestureDetector(
  //               onTap: () {
  //                 print('Code to open file manager');
  //               },
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.start,
  //                 crossAxisAlignment: CrossAxisAlignment.center,
  //                 children: [
  //                   Container(
  //                     width: 130,
  //                     height: 130,
  //                     margin: const EdgeInsets.only(bottom: 15),
  //                     decoration: BoxDecoration(
  //                       color: Colors.grey,
  //                       borderRadius: BorderRadius.circular(100),
  //                       // Profile Picture
  //                       image: const DecorationImage(
  //                           image: AssetImage('assets/profile/pp.png'),
  //                           fit: BoxFit.cover),
  //                     ),
  //                   ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: const [
  //                       Text('Change Profile Picture',
  //                           style: TextStyle(
  //                               fontFamily: 'inter',
  //                               fontWeight: FontWeight.w600,
  //                               color: Colors.white)),
  //                       SizedBox(width: 8),
  //                       Icon(Icons.camera,
  //                           color: Color.fromARGB(255, 150, 151, 159), size: 25),
  //                     ],
  //                   )
  //                 ],
  //               ),
  //             ),
  //           ),
  //           // Section 2 - User Info Wrapper
  //           Container(
  //             margin: const EdgeInsets.only(top: 24),
  //             width: MediaQuery.of(context).size.width,
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.start,
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               children: const [
  //                 UserInfoTile(
  //                   margin: const EdgeInsets.only(bottom: 16),
  //                   label: 'Nama Pengguna',
  //                   value: data['nama'],
  //                   padding: const EdgeInsets.symmetric(horizontal: 5),
  //                   valueBackground: const Color.fromARGB(255, 219, 213, 213),
  //                 ),
  //                 UserInfoTile(
  //                   margin: const EdgeInsets.only(bottom: 16),
  //                   label: 'Email',
  //                   value: data['email'],
  //                   padding: const EdgeInsets.symmetric(horizontal: 5),
  //                   valueBackground: const Color.fromARGB(255, 219, 213, 213),
  //                 ),
  //                 UserInfoTile(
  //                   margin: const EdgeInsets.only(bottom: 16),
  //                   label: 'Jenis Kelamin',
  //                   value: data['jenkl'],
  //                   valueBackground: const Color.fromARGB(255, 219, 213, 213),
  //                   padding: const EdgeInsets.symmetric(horizontal: 5),
  //                 ),
  //                 UserInfoTile(
  //                   margin: const EdgeInsets.only(bottom: 16),
  //                   label: 'Nomor Telepon',
  //                   value: data['hp'],
  //                   padding: const EdgeInsets.symmetric(horizontal: 5),
  //                   valueBackground: const Color.fromARGB(255, 219, 213, 213),
//                 ),
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
// --