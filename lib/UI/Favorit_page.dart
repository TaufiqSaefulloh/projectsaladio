// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:projectsaladio/Menu/minuman/menudrink.dart';
import 'package:projectsaladio/Menu/salad/menusalad.dart';
import 'package:projectsaladio/UTILS/addToFav.dart';

class FavoritesPage extends StatelessWidget {
  const FavoritesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[900],
        title: const Center(
          child: Text(
            'Favorites',
            style: TextStyle(fontWeight: FontWeight.normal),
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: favoritesItems.length + favoritesdrinks.length,
        itemBuilder: (context, index) {
          if (index < favoritesItems.length) {
            final favoritesItem = favoritesItems[index];
            final food =
                dummyFoods.firstWhere((food) => food.id == favoritesItem.id);

            return ListTile(
              leading: Image.asset(food.image),
              title: Text(food.name),
              subtitle: Text('Price: \$${food.price}'),
              trailing: IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () {
                  // Hapus produk dari keranjang belanja
                  removeFromFav(favoritesItem);
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: const Text('Success'),
                        content: const Text('Product removed from Favorites.'),
                        actions: [
                          TextButton(
                            child: const Text('OK'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            );
          } else {
            final favoritesIndex = index - favoritesItems.length;
            final favoritesDrink = favoritesdrinks[favoritesIndex];
            final drink =
                dummyDrinks.firstWhere((drink) => drink.id == favoritesDrink.id);

            return ListTile(
              leading: Image.asset(drink.image),
              title: Text(drink.name),
              subtitle: Text('Price: \$${drink.price}'),
              trailing: IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () {
                  // Remove drink from cart
                  removeFromFavDrink(favoritesDrink);
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: const Text('Success'),
                        content: const Text('Drink removed from Favorites.'),
                        actions: [
                          TextButton(
                            child: const Text('OK'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }
}
